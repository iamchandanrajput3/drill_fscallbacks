const fs = require('fs');

function createFilesThenDelete() {
    var arrOfNum = [1,2,3,4];
    var arrOfStr = ['hello', 'world'];
    const dirPath = '../output';

    fs.mkdir(dirPath, function(){
        fs.writeFile(`${dirPath}/first.json`, JSON.stringify(arrOfNum,null,2), (err)=>{
            fs.writeFile(`${dirPath}/second.json`, JSON.stringify(arrOfStr,null,2), (err)=>{
                fs.readdir(dirPath,(err,files) =>{
                    for(let file of files){
                        fs.rm(`${dirPath}/${file}`, (err) =>{
                            if(err) console.log(err);
                        });
                    }
                });
            });
        });
    });
}

module.exports = createFilesThenDelete;