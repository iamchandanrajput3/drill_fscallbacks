const fs = require('fs');


function problem2() {
    fs.readFile('../lipsum.txt', 'utf-8', (err,data)=>{
        const upperCase = data.toUpperCase();
        fs.writeFile('../upperCase.txt', upperCase, (err)=>{
            fs.writeFile('../filenames.txt','upperCase.txt\n',(err)=>{
                fs.readFile('../upperCase.txt', 'utf-8', (err,data)=>{
                    const lowerCase = data.toLowerCase().split('\n').filter((s)=>s.length!==0);
                    fs.writeFile('../lowerCase.txt', lowerCase.toString(), (err)=>{
                        fs.appendFile('../filenames.txt','lowerCase.txt\n', (err)=>{
                            fs.readFile('../lowerCase.txt','utf-8',(err,data)=>{
                                const sorted = data.split('').sort();
                                fs.writeFile('../sorted.txt', sorted.toString(), (err)=>{
                                    fs.appendFile('../filenames.txt','sorted.txt\n', (err)=>{
                                        fs.readFile('../filenames.txt','utf-8',(err,data)=>{
                                            const files = data.split('\n');
                                            files.forEach((file)=>{
                                                fs.rm(`../${file}`,(err)=>{});
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
}

module.exports = problem2;